package at.hpw.teleportplugin.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;

import org.bukkit.Location;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import at.hpw.teleportplugin.TeleportPlugin;
import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;

public class TeleportPluginGotoTest extends TeleportPluginBase {

    @Test
    public void testGotoSimple() {
        System.out.println("Running testGotoSimple");

        PlayerMock p1 = server.addPlayer();
        p1.setName("HaPeWe");
        p1.setLocation(new Location(server.getWorlds().get(0), 10, 10, 10));
        PlayerMock p2 = server.addPlayer();
        p2.setLocation(new Location(server.getWorlds().get(0), 20, 20, 20));
        p2.setName("OtherGamer");

        server.dispatchCommand(p1, "goto " + p2.getName() );

        String str;
        ArrayList<String> messages = getPlayerMessages(p1);

        assertEquals(1, messages.size(), "Expected received lines wrong");
        assertEquals("Going to " + p2.getDisplayName() + ".", messages.get(0), "Unexpected response");
    }

    @Test
    public void testGotoNonExistant() {
        System.out.println("Running testGotoNonExistant");

        PlayerMock p1 = server.addPlayer();
        p1.setName("HaPeWe");
 
        server.dispatchCommand(p1, "goto noob");

        String str;
        ArrayList<String> messages = getPlayerMessages(p1);

        assertEquals(2, messages.size(), "Expected received lines wrong");
        assertEquals("Player noob not found.", messages.get(0), "Unexpected output");
        assertEquals("No other players here.", messages.get(1), "Unexpected output");
    }

    @Test
    public void testGotoWrongTyped() {
        System.out.println("Running testGotoWrongTyped");

        server.setPlayers(20);
        PlayerMock p1 = server.addPlayer();
        p1.setName("HaPeWe");
        p1.setLocation(new Location(server.getWorlds().get(0), 10, 10, 10));
        PlayerMock p2 = server.addPlayer();
        p2.setLocation(new Location(server.getWorlds().get(0), 20, 20, 20));
        p2.setName("OtherGamer");


        server.dispatchCommand(p1, "goto OthaGamer");

        String str;
        ArrayList<String> messages = getPlayerMessages(p1);

        assertEquals(24, messages.size(), "Expected received lines wrong");
        assertEquals("Player OthaGamer not found.", messages.get(0), "Unexpected output");
        assertEquals("Available Players: ", messages.get(1), "Unexpected output");
    }

}
