package at.hpw.teleportplugin.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import be.seeseemelk.mockbukkit.entity.PlayerMock;

public class TeleportPluginListTest extends TeleportPluginBase {
    @Test
    public void testSendListSimple() {
        System.out.println("Running testSendListSimple");

        PlayerMock p1 = server.addPlayer();
        p1.setName("HaPeWe");
        PlayerMock p2 = server.addPlayer();

        server.dispatchCommand(p1, "list");

        String str;
        ArrayList<String> messages = getPlayerMessages(p1);

        assertEquals(3, messages.size(), "Expected received lines wrong");
        assertEquals(">> " + p2.getDisplayName(), messages.get(1), "Wrong Player name received");
        assertNotEquals(">> " + p1.getDisplayName(), messages.get(1), "My Own Player Name should not be included");
    }

    @Test
    public void testSendListMany() {
        System.out.println("Running testSendListMany");

        server.setPlayers(20);
        PlayerMock p1 = server.addPlayer();
        p1.setName("HaPeWe");
 
        server.dispatchCommand(p1, "list");

        String str;
        ArrayList<String> messages = getPlayerMessages(p1);

        assertEquals(22, messages.size(), "Expected received lines wrong");
        assertEquals("Available Players: ", messages.get(0), "Wrong initial line");
        for (String line : messages) {
            assertNotEquals(">> " + p1.getDisplayName(), line, "My Own Player Name should not be included");
        }
    }

    @Test
    public void testSendListAlone() {
        System.out.println("Running testSendListAlone");

        PlayerMock p1 = server.addPlayer();
        p1.setName("HaPeWe");
    
        server.dispatchCommand(p1, "list");

        String str;
        ArrayList<String> messages = getPlayerMessages(p1);

        assertEquals(1, messages.size(), "Expected received lines wrong");
        assertEquals("No other players here.", messages.get(0), "Wrong Status message");
    }
}