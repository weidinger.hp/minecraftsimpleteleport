package at.hpw.teleportplugin.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

import at.hpw.playerlistener.MyPlayerListener;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;

import be.seeseemelk.mockbukkit.entity.PlayerMock;
@Disabled("Do Not Know how to do a respawn automatically yet")
final class PlayerKilledEventTest extends TeleportPluginBase {

    @Test
    @DisplayName("PlayerKilledWithItemsNoKeep")
    public void testPlayerKilledWithItemsNoKeep() {
        System.out.println("Executing testPlayerKilledWithItems");
        MyPlayerListener.TEST_ENABLE_KEEP = false; // TEST Code Only

        PlayerMock p1 = server.addPlayer();
		p1.setName("HaPeWe");

        runPlayerKill(p1);

        assertNotEquals(Material.STONE_SWORD, p1.getInventory().getItem(0).getType());
        assertNotEquals(1, p1.getInventory().getItem(0).getAmount());
        assertNotEquals(Material.BREAD, p1.getInventory().getItem(1).getType());
        assertNotEquals(5, p1.getInventory().getItem(1).getAmount());
    }

    @Test
    @DisplayName("PlayerKilledWithItemsKeep")
    public void testPlayerKilledWithItems() {
        System.out.println("Executing testPlayerKilledWithItems");
        MyPlayerListener.TEST_ENABLE_KEEP = true; // TEST Code Only

        PlayerMock p1 = server.addPlayer();
		p1.setName("HaPeWe");

        runPlayerKill(p1);

        assertEquals(Material.STONE_SWORD, p1.getInventory().getItem(0).getType());
        assertEquals(1, p1.getInventory().getItem(0).getAmount());
        assertEquals(Material.BREAD, p1.getInventory().getItem(1).getType());
        assertEquals(5, p1.getInventory().getItem(1).getAmount());
    }

    private void runPlayerKill(PlayerMock p1) {
    
        PlayerMock killer = server.addPlayer();

        ItemStack stoneSword = new ItemStack(Material.STONE_SWORD);
        stoneSword.setAmount(1);
        ItemStack bread = new ItemStack(Material.BREAD);
        bread.setAmount(5);
 
        p1.getInventory().addItem(stoneSword, bread);


        p1.setItemInHand(stoneSword);

        assertEquals(Material.STONE_SWORD, p1.getInventory().getItem(0).getType());
        assertEquals(1, p1.getInventory().getItem(0).getAmount());
        assertEquals(Material.BREAD, p1.getInventory().getItem(1).getType());
        assertEquals(5, p1.getInventory().getItem(1).getAmount());

        while (p1.getHealth() > 0) {
            p1.damage(10, killer);
        }

        assertEquals(p1.getHealth(), 0);

        getPlayerMessages(p1);

        // need to do a respawn here - dont know yet how to do this
    }
}