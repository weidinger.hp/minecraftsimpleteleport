package at.hpw.teleportplugin.test;

import java.util.ArrayList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import at.hpw.teleportplugin.TeleportPlugin;
import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;

public class TeleportPluginBase {
    protected ServerMock server;
    TeleportPlugin plugin;

    
    // want to have a clean server after every run
    @BeforeEach
    public void before() {
        server = MockBukkit.mock();
        plugin = (TeleportPlugin) MockBukkit.load(TeleportPlugin.class);
    }

    @AfterEach
    public void after() {
        MockBukkit.unload();
    }

	protected ArrayList<String> getPlayerMessages(PlayerMock player) {
		String str;
		ArrayList<String> messages = new ArrayList<>();

        while ((str = player.nextMessage()) != null) {
            messages.add(str);
            System.out.println("[msg] " + str);
        }
        return messages;
    }

}