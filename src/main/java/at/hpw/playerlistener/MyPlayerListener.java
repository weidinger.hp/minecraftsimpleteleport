package at.hpw.playerlistener;

import java.util.List;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class MyPlayerListener implements Listener {
    public static boolean TEST_ENABLE_KEEP = true;

    JavaPlugin plugin;

    public MyPlayerListener(JavaPlugin plugin) {
        this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.NORMAL)
    public void onDied(PlayerDeathEvent event) {
        event.getEntity().sendMessage("You have died - i am protecting your Inventory.");
        List<ItemStack> drops = event.getDrops();
        event.setKeepInventory(TEST_ENABLE_KEEP);
        event.setKeepLevel(TEST_ENABLE_KEEP);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onRespawn(PlayerRespawnEvent event) {
        event.getPlayer().sendMessage("You have respawned - please check you inventory.");
    }
}