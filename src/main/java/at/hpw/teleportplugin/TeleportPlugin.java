package at.hpw.teleportplugin;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import at.hpw.playerlistener.MyPlayerListener;

public final class TeleportPlugin extends JavaPlugin {
    public TeleportPlugin() {
        super();
    }
    
    public TeleportPlugin(  JavaPluginLoader loader,
                            PluginDescriptionFile description,
                            java.io.File dataFolder,
                            java.io.File file) {
        super(loader, description, dataFolder, file);
    }

    @Override
    public void onEnable() {
        getCommand("goto").setExecutor((sender, command, label, args) -> TeleportTo(sender, command, label, args));
        getCommand("list").setExecutor((CommandSender sender, Command command, String label, String[] args) -> { 
            printPlayerList(sender);
            return true; });

        getServer().getPluginManager().registerEvents(new MyPlayerListener(this), this);
    }

    private boolean TeleportTo(CommandSender sender, Command command, String label, String[] args) {
        if (args == null || args.length != 1 || !(sender instanceof Player)) {
            sender.sendMessage(command.getUsage());
            return false;
        }
        Player target = this.getServer().getPlayer(args[0]);
        if (target == null) {
            sender.sendMessage("Player " + args[0] + " not found.");
            printPlayerList(sender);
            return true;
        }
        sender.sendMessage("Going to " + target.getDisplayName() + ".");

        ((Player) sender).teleport(target);
        return true;
    }

    /** Prints the list of "other" players to the sender */
    private void printPlayerList(CommandSender sender) {
        ArrayList<String> playerList = new ArrayList<>();
        for( Player p : getServer().getOnlinePlayers() ) {
            if (p.equals(sender)) continue; // dont add myself to the list
            playerList.add(p.getName());
        }

        if (playerList.isEmpty()) {
            sender.sendMessage("No other players here.");
            return;
        }
        
        sender.sendMessage("Available Players: ");
        for (String playerName : playerList) {
            sender.sendMessage(">> " + playerName);
        }
        sender.sendMessage("========================");
    }
    
}